const { src, dest } = require('gulp');

const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const size = require('gulp-size');
const imagemin = require('gulp-imagemin');
const newer = require('gulp-newer');
const webp = require('gulp-webp');
const if_ = require('gulp-if');

// Конфигурация
const path = require('../config/path.js');
const app = require('../config/app.js');

// Обработка Img
const img = () => {
    return src(path.img.src)
        // .pipe()
        .pipe(plumber({
            errorHandler: notify.onError(error => ({
                title: 'Img',
                massage: error.message
            }))
        }))
        .pipe(newer(path.img.dest))// Не обрабатывать уже обработанные изображения
        .pipe(webp())// Использование формата webp
        .pipe(dest(path.img.dest))// Сохранить

        .pipe(src(path.img.src))
        .pipe(newer(path.img.dest))// Не обрабатывать уже обработанные изображения
        .pipe(imagemin(app.imagemin))// Сжать изображения
        .pipe(dest(path.img.dest))// Сохранить
}

module.exports = img;

