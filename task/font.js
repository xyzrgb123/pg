const { src, dest } = require('gulp');

const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const newer = require('gulp-newer');
const fonter = require('gulp-fonter-unx');
const tt2woff2 = require('gulp-ttf2woff2');
const if_ = require('gulp-if');

// Конфигурация
const path = require('../config/path.js');
const app = require('../config/app.js');

// Обработка Font
const font = () => {
    return src(path.font.src)
        .pipe(plumber({
            errorHandler: notify.onError(error => ({
                title: 'Font',
                massage: error.message
            }))
        }))
        .pipe(newer(path.font.dest))
        .pipe(fonter(app.fonter))
        .pipe(dest(path.font.dest))
        .pipe(tt2woff2())
        .pipe(dest(path.font.dest))
}

module.exports = font;

