const { src, dest } = require('gulp');

const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const size = require('gulp-size');
const stylus_ = require('gulp-stylus');//(require('stylus'))
const autoprefixer = require('gulp-autoprefixer')
const csso = require('gulp-csso')
const rename = require('gulp-rename')
const shorthand = require('gulp-shorthand')
const groupCssMediaQueries = require('gulp-group-css-media-queries')
const webpCss = require('gulp-webp-css');
const if_ = require('gulp-if');

// Конфигурация
const path = require('../config/path.js');
const app = require('../config/app.js');

// Обработка Stylus
const stylus = () => {
    return src(path.stylus.src)
        .pipe(plumber({
            errorHandler: notify.onError(error => ({
                title: 'Stylus',
                massage: error.message
            }))
        }))
        .pipe(stylus_(app.stylus))// Компиляция Stylus -> CSS
        // .pipe(())
        .pipe(webpCss())
        .pipe(groupCssMediaQueries())// Объединение медиа запросов
        .pipe(if_(app.isProd, shorthand()))// Замена длинного синтаксиса на короткий
        .pipe(autoprefixer())// Префиксы
        .pipe(dest(path.stylus.dest, { sourcemaps: app.isDev }))// Сохранение
        .pipe(rename({ suffix: '.min' }))// Добавить суффикс .min
        .pipe(csso())// Минимизировать файл
        .pipe(size({ title: 'main.min.css' }))// Вывести в терминал размер
        .pipe(dest(path.stylus.dest))// Снова сохранить (но уже минимизированный и с суффиксом)
}

module.exports = stylus;

