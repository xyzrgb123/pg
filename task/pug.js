const { src, dest } = require('gulp');

const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const htmlmin = require('gulp-htmlmin');
const size = require('gulp-size');
const pugs = require('gulp-pug');
const webpHtml = require('gulp-webp-html');
const if_ = require('gulp-if');

// Конфигурация
const path = require('../config/path.js');
const app = require('../config/app.js');

// Обработка Pug
const pug = () => {
    return src(path.pug.src)
        .pipe(plumber({
            errorHandler: notify.onError(error => ({
                title: 'Pug',
                massage: error.message
            }))
        }))
        // .pipe(())
        .pipe(pugs(app.pug))// Компиляция Pug -> HTML
        .pipe(webpHtml())// Замена webp для старых браузеров
        .pipe(dest(path.pug.dest, { sourcemaps: app.isDev }))// Сохранение
}

module.exports = pug;

