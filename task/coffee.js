const { src, dest } = require('gulp');

const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const size = require('gulp-size');
const coffee_ = require('gulp-coffee');
const rename = require('gulp-rename')
const uglify = require('gulp-uglify')
const if_ = require('gulp-if');

// Конфигурация
const path = require('../config/path.js');
const app = require('../config/app.js');

// Обработка Coffee
const coffee = () => {
    return src(path.coffee.src)
        .pipe(plumber({
            errorHandler: notify.onError(error => ({
                title: 'Coffee',
                massage: error.message
            }))
        }))
        .pipe(coffee_(app.coffee))// Компиляция Coffee -> js
        .pipe(dest(path.coffee.dest, { sourcemaps: app.isDev }))// Сохранение
        .pipe(rename({ suffix: '.min' }))// Добавить суффикс .min
        .pipe(uglify())// Минимизировать
        .pipe(size({ title: 'main.min.js' }))// Вывести размер в терминал
        .pipe(dest(path.coffee.dest))// Снова сохранить (но уже минимизированный и с суффиксом)
}

module.exports = coffee;

