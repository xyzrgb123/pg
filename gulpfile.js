// КОНФИГУРАЦИЯ
// {{{1
const pathSrc = './src';
const pathDest = './public';

path = {
    root: pathDest,
    pug: {
        src: pathSrc + '/*.pug',
        watch: pathSrc + '/**/*.pug',
        dest: pathDest
    },
    stylus: {
        src: pathSrc + '/*.styl',
        watch: pathSrc + '/**/*.styl',
        dest: pathDest
    },
    coffee: {
        src: pathSrc + '/*.coffee',
        watch: pathSrc + '/**/*.coffee',
        dest: pathDest
    },
    img: {
        src: pathSrc + '/img/*.{png,jpg,jpeg,svg,gif}',
        watch: pathSrc + '/img/**/*.{png,jpg,jpeg,svg,gif}',
        dest: pathDest + '/img'
    },
    font: {
        src: pathSrc + '/font/*.{eot,ttf,otc,ttc,woff,woff2,svg}',
        watch: pathSrc + '/font/**/*.{eot,ttf,otc,ttc,woff,woff2,svg}',
        dest: pathDest + '/font'
    },
}

const isProd = process.argv.includes('--production')
const isDev = !isProd

const { src, dest, watch, series, parallel } = require('gulp')
const browserSync = require('browser-sync').create()
const del = require('del')
const gp = require('gulp-load-plugins')()
// }}}

// ЗАДАЧИ
// {{{1 Clear
const clear = () => {
    return del(path.root)
}
exports.clear = clear

// {{{1 Pug
const pug = () => {
    return src(path.pug.src)
        .pipe(gp.plumber({ errorHandler: gp.notify.onError(error => ({ title: 'Pug', massage: error.message })) }))
        .pipe(gp.pug({ pretty: true }))// Компиляция Pug -> HTML
        .pipe(gp.webpHtml())// Замена webp для старых браузеров
        .pipe(dest(path.pug.dest, { sourcemaps: isDev }))// Сохранение
}
exports.pug = pug

// {{{1 Stylus
const stylus = () => {
    return src(path.stylus.src)
        .pipe(gp.plumber({ errorHandler: gp.notify.onError(error => ({ title: 'Stylus', massage: error.message })) }))
        .pipe(gp.stylus())// Компиляция Stylus -> CSS
        .pipe(gp.webpCss())// Добавляем webp
        .pipe( gp.if(isProd, gp.groupCssMediaQueries()) )// Объединение медиа запросов
        .pipe( gp.if(isProd, gp.shorthand()) )// Замена длинного синтаксиса на короткий
        .pipe(gp.autoprefixer())// Префиксы
        .pipe(dest(path.stylus.dest, { sourcemaps: isDev }))// Сохранение

        .pipe( gp.if(isProd, gp.rename({ suffix: '.min' })) )// Добавить суффикс .min
        .pipe( gp.if(isProd, gp.csso()) )// Минимизировать файл
        .pipe( gp.if(isProd, dest(path.stylus.dest)) )// Снова сохранить (но уже минимизированный и с суффиксом)
}
exports.stylus = stylus

// {{{1 Coffee
const coffee = () => {
    return src(path.coffee.src)
        .pipe(gp.plumber({ errorHandler: gp.notify.onError(error => ({ title: 'Coffee', massage: error.message })) }))
        .pipe(gp.coffee(coffee))// Компиляция Coffee -> js
        .pipe(dest(path.coffee.dest, { sourcemaps: isDev }))// Сохранение

        .pipe( gp.if( isProd, gp.rename({ suffix: '.min' })) )// Добавить суффикс .min
        .pipe( gp.if( isProd, gp.uglify()) )// Минимизировать
        .pipe( gp.if( isProd, dest(path.coffee.dest)) )// Снова сохранить (но уже минимизированный и с суффиксом)
}
exports.coffee = coffee

// {{{1 Img
const img = () => {
    return src(path.img.src)
        .pipe(gp.plumber({ errorHandler: gp.notify.onError(error => ({ title: 'Img', massage: error.message })) }))
        .pipe(gp.newer(path.img.dest))// Не обрабатывать уже обработанные изображения
        .pipe(gp.webp())// Использование формата webp
        .pipe(dest(path.img.dest))// Сохранить

        .pipe(src(path.img.src))
        .pipe(gp.newer(path.img.dest))// Не обрабатывать уже обработанные изображения
        .pipe(gp.imagemin({ verbose: true }))// Сжать изображения
        .pipe(dest(path.img.dest))// Сохранить
}
exports.img = img

// {{{1 Font
const font = () => {
    return src(path.font.src)
        .pipe(gp.plumber({ errorHandler: gp.notify.onError(error => ({ title: 'Font', massage: error.message })) }))
        .pipe(gp.newer(path.font.dest))// не обрабатываем то что уже обработано
        .pipe(gp.fonterUnx({ formats: ['ttf', 'woff', 'eot', 'svg', 'otf'] }))// Конвертируем шрифты в указанные форматы
        .pipe(dest(path.font.dest))// сохраняем

        .pipe(gp.ttf2woff2())// конвертируем в woff2
        .pipe(dest(path.font.dest))// сохраняем
}
exports.font = font
// }}}

//  ПОЕХАЛИ
// {{{1 Server
const server = () => {
    browserSync.init({
        server: {
            baseDir: path.root
        }
    })
}

// {{{1 Watch
const watcher = () => {
    watch(path.pug.watch, pug).on('all', browserSync.reload)
    watch(path.stylus.watch, stylus).on('all', browserSync.reload)
    watch(path.coffee.watch, coffee).on('all', browserSync.reload)
    watch(path.img.watch, img).on('all', browserSync.reload)
    watch(path.font.watch, font).on('all', browserSync.reload)
}
exports.watch = watcher

// {{{1 Build
const build = series(
    clear,
    parallel(pug, stylus, coffee, img, font)
);

const dev = series(
    build,
    parallel(watcher, server)
);

exports.default = isProd
    ? build
    : dev
