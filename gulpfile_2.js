// CONFIG
// {{{1
const pathSrc = './src';
const pathDest = './public';

path = {
    root: pathDest,
    pug: {
        src: pathSrc + '/*.pug',
        watch: pathSrc + '/**/*.pug',
        dest: pathDest
    },
    stylus: {
        src: pathSrc + '/*.styl',
        watch: pathSrc + '/**/*.styl',
        dest: pathDest
    },
    coffee: {
        src: pathSrc + '/*.coffee',
        watch: pathSrc + '/**/*.coffee',
        dest: pathDest
    },
    img: {
        src: pathSrc + '/img/*.{png,jpg,jpeg,svg,gif}',
        watch: pathSrc + '/img/**/*.{png,jpg,jpeg,svg,gif}',
        dest: pathDest + '/img'
    },
    font: {
        src: pathSrc + '/font/*.{eot,ttf,otc,ttc,woff,woff2,svg}',
        watch: pathSrc + '/font/**/*.{eot,ttf,otc,ttc,woff,woff2,svg}',
        dest: pathDest + '/font'
    },
}

const isProd = process.argv.includes('--production')
const isDev = !isProd

// }}}

// ПОДКЛЮЧЕНИЕ
// {{{1
const { src, dest, watch, series, parallel } = require('gulp')
const browserSync = require('browser-sync').create()
const if_ = require('gulp-if')
const del = require('del')

const plumber = require('gulp-plumber')
const notify = require('gulp-notify')
const htmlmin = require('gulp-htmlmin')
const size = require('gulp-size')
const pugs = require('gulp-pug')
const webpHtml = require('gulp-webp-html')

const stylus_ = require('gulp-stylus')
const autoprefixer = require('gulp-autoprefixer')
const csso = require('gulp-csso')
const rename = require('gulp-rename')
const shorthand = require('gulp-shorthand')
const groupCssMediaQueries = require('gulp-group-css-media-queries')
const webpCss = require('gulp-webp-css')

const coffee_ = require('gulp-coffee')
const uglify = require('gulp-uglify')

const imagemin = require('gulp-imagemin')
const newer = require('gulp-newer')
const webp = require('gulp-webp')

const fonter = require('gulp-fonter-unx')
const tt2woff2 = require('gulp-ttf2woff2')
// }}}

// ЗАДАЧИ
// {{{1 Clear
const clear = () => {
    return del(path.root)
}
exports.clear = clear

// {{{1 Pug
const pug = () => {
    return src(path.pug.src)
        .pipe(plumber({
            errorHandler: notify.onError(error => ({
                title: 'Pug',
                massage: error.message
            }))
        }))
        .pipe(pugs({ pretty: true }))// Компиляция Pug -> HTML
        .pipe(webpHtml())// Замена webp для старых браузеров
        .pipe(dest(path.pug.dest, { sourcemaps: isDev }))// Сохранение
}
exports.pug = pug

// {{{1 Stylus
const stylus = () => {
    return src(path.stylus.src)
        .pipe(plumber({
            errorHandler: notify.onError(error => ({
                title: 'Stylus',
                massage: error.message
            }))
        }))
        .pipe(stylus_())// Компиляция Stylus -> CSS
        .pipe(webpCss())
        .pipe(groupCssMediaQueries())// Объединение медиа запросов
        .pipe(if_(isProd, shorthand()))// Замена длинного синтаксиса на короткий
        .pipe(autoprefixer())// Префиксы
        .pipe(dest(path.stylus.dest, { sourcemaps: isDev }))// Сохранение
        .pipe(rename({ suffix: '.min' }))// Добавить суффикс .min
        .pipe(csso())// Минимизировать файл
        .pipe(size({ title: 'main.min.css' }))// Вывести в терминал размер
        .pipe(dest(path.stylus.dest))// Снова сохранить (но уже минимизированный и с суффиксом)
}
exports.stylus = stylus

// {{{1 Coffee
const coffee = () => {
    return src(path.coffee.src)
        .pipe(plumber({
            errorHandler: notify.onError(error => ({
                title: 'Coffee',
                massage: error.message
            }))
        }))
        .pipe(coffee_(coffee))// Компиляция Coffee -> js
        .pipe(dest(path.coffee.dest, { sourcemaps: isDev }))// Сохранение
        .pipe(rename({ suffix: '.min' }))// Добавить суффикс .min
        .pipe(uglify())// Минимизировать
        .pipe(size({ title: 'main.min.js' }))// Вывести размер в терминал
        .pipe(dest(path.coffee.dest))// Снова сохранить (но уже минимизированный и с суффиксом)
}
exports.coffee = coffee

// {{{1 Img
const img = () => {
    return src(path.img.src)
        .pipe(plumber({
            errorHandler: notify.onError(error => ({
                title: 'Img',
                massage: error.message
            }))
        }))
        .pipe(newer(path.img.dest))// Не обрабатывать уже обработанные изображения
        .pipe(webp())// Использование формата webp
        .pipe(dest(path.img.dest))// Сохранить

        .pipe(src(path.img.src))
        .pipe(newer(path.img.dest))// Не обрабатывать уже обработанные изображения
        .pipe(imagemin({ verbose: true }))// Сжать изображения
        .pipe(dest(path.img.dest))// Сохранить
}
exports.img = img

// {{{1 Font
const font = () => {
    return src(path.font.src)
        .pipe(plumber({
            errorHandler: notify.onError(error => ({
                title: 'Font',
                massage: error.message
            }))
        }))
        .pipe(newer(path.font.dest))
        .pipe(fonter({ formats: ['ttf', 'woff', 'eot', 'svg', 'otf'] }))
        .pipe(dest(path.font.dest))
        .pipe(tt2woff2())
        .pipe(dest(path.font.dest))
}
exports.font = font
// }}}

//  GO!
// {{{1 Server
const server = () => {
    browserSync.init({
        server: {
            baseDir: path.root
        }
    })
}

// {{{1 Watch
const watcher = () => {
    watch(path.pug.watch, pug).on('all', browserSync.reload)
    watch(path.stylus.watch, stylus).on('all', browserSync.reload)
    watch(path.coffee.watch, coffee).on('all', browserSync.reload)
    watch(path.img.watch, img).on('all', browserSync.reload)
    watch(path.font.watch, font).on('all', browserSync.reload)
}
exports.watch = watcher

// {{{1 Build
const build = series(
    clear,
    parallel(pug, stylus, coffee, img, font)
);

const dev = series(
    build,
    parallel(watcher, server)
);

exports.default = isProd
    ? build
    : dev
